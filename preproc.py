#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 11:20:44 2020

@author: Jake Drysdale
"""
import numpy as np
import tensorflow as tf

class Preproc(object):
    def __init__(self, path, size=4096):
        self._path = path
        self._size = size
        self._raw_audio = self.load_npy()
        self._fade_audio = self.fade_audio(self._raw_audio)
        self._pad_audio = self.pad_audio(self._fade_audio)
        self.x_train = self.preproc(self._pad_audio)

    def  load_npy(self):
        return np.load(self._path)
        
    def fade_audio(self, audio):
        """
        Maybe add very short initial fade
        """
        faded_audio=[]
        fade_length = int(len(audio[0])*0.2)
        for i in range(len(audio)):
            xfade = np.hstack((np.ones(len(audio[i])-fade_length), 
                               np.linspace(1,0,fade_length)))
            faded_audio=audio*xfade
            
        return faded_audio
    
    def pad_audio(self, audio):
        pad_size = self._size-len(audio[0])
        pad = np.zeros(pad_size)
        padded_audio=[]
        for i in range(len(audio)):
            padded = np.append(audio[i],pad)
            padded_audio.append(padded)
        
        return np.array(padded_audio)
    
    def preproc(self, audio):
        audio = self.fade_audio(audio)      
        audio = self.pad_audio(audio)
        audio =  [np.flip(x) for  x in audio]
        audio = np.array(audio)
        x_train = tf.convert_to_tensor(audio) 
        x_train = (tf.expand_dims(x_train, axis=-1))
        
        return x_train
    


path1 = '/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/audio_data/kick_low_16k_4k.npy'
path2 = '/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/audio_data/kick_mid_16k_4k.npy'
path3 = '/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/audio_data/kick_high_16k_4k.npy'

#low frequencies
preproc=Preproc(path1)
low = preproc.x_train

#mid frqeuencies
preproc2=Preproc(path2)
mid = preproc2.x_train

#high frequencies
preproc3=Preproc(path3)
high = preproc3.x_train

#concat
x_train = tf.concat([low,mid,high],-1)
    
