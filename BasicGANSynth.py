#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 14:50:43 2020

@author: jakedrysdale
"""


from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import UpSampling2D, Conv2D, Activation, Input
from tensorflow.keras.layers import Dense, Reshape, AveragePooling2D, LeakyReLU, Flatten
from tensorflow.keras.optimizers import RMSprop
import tensorflow.keras.backend as K

import numpy as np
from functools import partial



class Gan(object):
    
    def __init__(self, x_train, latent_size=64, batch_size=16, iterations=50000, 
                 lr=8e-4, start_h=16, start_w=2, end_h=1024, end_w=128, 
                 n_filters=64, n_chan=1):
        #data
        self._x_train = x_train
        
        #parameters
        self._latent_size = latent_size
        self._batch_size = batch_size
        self._iterations = iterations
        self._lr = lr
        
        #spec/image params
        self._start_h = start_h
        self._start_w = start_w
        self._end_h = end_h
        self._end_w = end_w
        self._n_filters = n_filters
        self._n_chan = n_chan
        
        #network building
        self._generator = self.make_generator(self._latent_size)
        self._discriminator = self.make_discriminator()    
        self._DiscriminatorModel = self.build_d_network()
        self._GeneratorModel = self.build_g_network() 
        
        #losses
        self._d_loss = []
        self._g_loss = []
        self._gp_loss = []
        

    def wasserstein_loss(self, y_true, y_pred):
        """
        Wasserstein_loss (default loss has been set to 'mean_squared_error')
        """
        return K.mean(y_true * y_pred)
    

    def r1_gradient_penalty(self, y_true, y_pred, samples, sample_weight):
        """
        Helps prevent exploding gradients and mode collapse issues:
            "Stops the discriminator from telling the generator to change 
            the generated pixel values by too much"
        """
        gradients = K.gradients(y_pred, samples)[0]
        gradients_sqr = K.square(gradients)
        gradient_penalty = K.sum(gradients_sqr,
                                  axis=np.arange(1, len(gradients_sqr.shape)))
        return K.mean(gradient_penalty)
    
#    def pixel_norm(self, x, epsilon=1e-8):
#        epsilon = tf.constant(epsilon, dtype=x.dtype, name='epsilon')
#        return x * tf.math.rsqrt(tf.reduce_mean(tf.square(x), axis=1, keepdims=True) + epsilon)

    def g_block(self, input_tensor, filters):
        out = UpSampling2D()(input_tensor)
        out = Conv2D(filters, 3, padding = 'same')(out)
        out = Activation('relu')(out)
        return out
    
    
    def d_block(self, input_tensor, filters):
        out = Conv2D(filters, 3, padding = 'same')(input_tensor)
        out = LeakyReLU(0.2)(out)
        out = AveragePooling2D()(out)
        return out


    def make_generator(self, latent_size):
        #latent input
        latent_input = Input([latent_size])
        
        #reshape to 2x16x_n_filters
        x = Dense(self._n_filters*self._start_h*self._start_w, activation = 'relu')(latent_input)
        x = Reshape([self._start_h, self._start_w, self._n_filters])(x)
         
        #size: 32x4x_n_filters
        x = self.g_block(x, self._n_filters)
        
        #size: 64x8x_n_filters
        x = self.g_block(x, self._n_filters)
        
        #size: 128x16x_n_filters
        x = self.g_block(x, self._n_filters)
    
        #size: 256x32x_n_filters//2
        x = self.g_block(x, self._n_filters//2)
        
        #size: 512x64x_n_filters//4
        x = self.g_block(x, self._n_filters//4)
    
        #size: 1024x128x_n_filters//8
        x = self.g_block(x, self._n_filters//8)
        
        #size: 64x64x8, make RGB with values between 0 and 1
        image_output = Conv2D(self._n_chan, 1, padding = 'same', activation = 'sigmoid')(x)
        
        #make model
        generator = Model(inputs = latent_input, outputs = image_output)
        
        return generator


    def make_discriminator(self):
        #image input
        image_input = Input([self._end_h, self._end_w, self._n_chan])
        
        #size: 1024x128x_n_filters//8
        x = self.d_block(image_input, self._n_filters//8)
        
        #size: 512x64x_n_filters//4
        x = self.d_block(x, self._n_filters//4)
        
        #size: 256x32x_n_filters//2
        x = self.d_block(x, self._n_filters//2)
        
        #size: 128x16x_n_filters
        x = self.d_block(x, self._n_filters)
        
        #size: 64x8x_n_filters
        x = self.d_block(x, self._n_filters)
        
        #size: 32x4x_n_filters
        x = self.d_block(x, self._n_filters)
    
        #size: 16x2x64
        x = Conv2D(self._n_filters, 3, padding = 'same')(x)
        x = LeakyReLU(0.2)(x)
        x = Flatten()(x)
        
        #1-dimensional neural network
        class_output = Dense(1)(x)
        
        #make model
        discriminator = Model(inputs = image_input, outputs = class_output)
        
        return discriminator
    
    
    def model_summary(self):
        self._generator.summary()
        self._discriminator.summary()
    
    
# =============================================================================
# TRAINING
# =============================================================================
    def build_d_network(self):
        
        loss_f = 'mean_squared_error'
#        loss_f = self.wasserstein_loss 
        
        #build network to train the discriminator
        #discriminator will train, but generator won't train
        for layer in self._discriminator.layers:
            layer.trainable = True
            
        for layer in self._generator.layers:
            layer.trainable = False
            
        #get real image
        real_image = Input([self._end_h, self._end_w, self._n_chan])
        
        #discriminator classifies
        validity_real = self._discriminator(real_image)
        
        #get latent input
        latent_input = Input([self._latent_size])
        
        #generate an image
        fake_image =self._generator(latent_input)
        
        #discriminator classifies
        validity_fake = self._discriminator(fake_image)
        
        #create gradient penalty loss with real samples
        partial_gp = partial(self.r1_gradient_penalty, samples = real_image)
        
        #create and compile the model
        DiscriminatorModel = Model(inputs = [real_image, latent_input], 
                                   outputs = [validity_real, validity_fake, 
                                              validity_real])
        
        DiscriminatorModel.compile(optimizer = RMSprop(lr = self._lr),
                                   loss = [loss_f, 
                                           loss_f, partial_gp], 
                                           loss_weights = [1, 1, 10])
        return DiscriminatorModel


    def build_g_network(self):
        
        loss_f = 'mean_squared_error'
#        loss_f = self.wasserstein_loss

        #build network to train the generator
        #discriminator won't train, but generator will train
        for layer in self._discriminator.layers:
            layer.trainable = False
            
        for layer in self._generator.layers:
            layer.trainable = True
        
        #get latent input
        latent_input = Input([self._latent_size])
        
        #generate an image
        fake_image = self._generator(latent_input)
        
        #discriminator classifies
        validity = self._discriminator(fake_image)
        
        #create and compile the model
        GeneratorModel = Model(inputs = latent_input, outputs = validity)
        GeneratorModel.compile(optimizer = RMSprop(lr = self._lr), 
                               loss = loss_f)
        return GeneratorModel


    def train_loop(self):
        #train the models in a loop
        for i in range(self._iterations):
            
            print('\rIteration ' + str(i), end = '')
            
            #get labels
            real_labels = np.ones([self._batch_size, 1])
            fake_labels = np.zeros([self._batch_size, 1])
            dummy_labels = np.ones([self._batch_size, 1])
            
            #train discriminator
            #get images and latent vectors
            image_indices = np.random.randint(0, self._x_train.shape[0] - 1, 
                                              [self._batch_size])
        #    real_images = x_train[image_indices]
        
            real_images=[]
            for i in range(len(image_indices)):
                real_images.append(self._x_train[image_indices[i]])
                
            real_images = tf.convert_to_tensor(real_images)
            latent_vectors = np.random.normal(0.0, 1.0, [self._batch_size, self._latent_size])
            
            #train
            loss = self._DiscriminatorModel.train_on_batch([real_images, latent_vectors], 
                                                           [real_labels, fake_labels, 
                                                            dummy_labels])
            self._d_loss.append(loss[1]/2 + loss[2]/2)
            self._gp_loss.append(loss[3])
            
            #train generator
            #get latent vectors
            latent_vectors = np.random.normal(0.0, 1.0, [self._batch_size, self._latent_size])
            
            #train using opposite labels
            loss = self._GeneratorModel.train_on_batch(latent_vectors, 
                                                       real_labels)
            self._g_loss.append(loss)
            
    #    with train_summary_writer.as_default():
    #      tf.summary.scalar('disc_loss', d_loss, step=i)
    #      tf.summary.scalar('gen_loss', g_loss, step=i)
    
    def plot_losses(self):
        
        # plot losses
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.plot(self.d_loss,label='d_loss')
    #    ax2 = fig.add_subplot(111)
        ax1.plot(self.g_loss,label='g_loss')
        plt.legend()
        plt.title('Generator and Discriminator Losses')
        plt.xlabel('Iterations')
        plt.ylabel('Loss')
        
#        now = datetime.now()
#        current_time = now.strftime("%H:%M:%S")
#        loss_dir = '/Users/s15125210/Documents/PhD/gitlab/kickGAN/loss_graphs/'
#        fig.savefig(loss_dir+current_time+'_dg_loss'+'.png')








'x_train = x_train[batch_size,height,width,channels("e.g. RBG")]'
specs = tf.convert_to_tensor(demo)
x_train = specs
gan = Gan(x_train)
summary = gan.model_summary()

train = gan.train_loop() #run GAN training on x_train








# generate samples
gen_amount=1000
latent_vectors = np.random.normal(0.0, 1.0, [gen_amount, gan._latent_size])
fake_images = gan._generator.predict(latent_vectors)

generations_dir= '/Users/s15125210/Documents/PhD/data-augmentation-gan/generations/'

np.save(generations_dir+current_time+'.npy', fake_images)


# show samples
for i in range(30):
    rand_indx = np.random.randint(len(fake_images))
    plt.figure(i)
    show(fake_images[rand_indx,:,:,0])



#from matplotlib import pyplot as plt
#from datetime import datetime



loss_dir = '/Users/s15125210/Documents/PhD/data-augmentation-gan/loss/'

# plot losses
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(gan._d_loss,label='d_loss')

ax2 = fig.add_subplot(111)
ax1.plot(gan._g_loss,label='g_loss')
plt.legend()
plt.title('Generator and Discriminator Losses')
plt.xlabel('Iterations')
plt.ylabel('Loss')

fig.savefig(loss_dir+current_time+'_dg_loss'+'.png')



fig2 = plt.figure()
ax1 = fig2.add_subplot(111)
ax1.plot(gan._gp_loss,label='gp_loss')
plt.legend()
plt.title('Gradient Penalty Loss')
plt.xlabel('Iterations')
plt.ylabel('Loss')
#
fig2.savefig(loss_dir+current_time+'_gp_loss'+'.png')
#












# =============================================================================
# 
# =============================================================================

"""
#set up summary writers to write the summaries to disk in a different logs directory:
current_time = datetime.now().strftime("%Y%m%d-%H%M%S")
#cwd = os.getcwd()
train_log_dir = './logs/' + current_time + '/train'
train_summary_writer = tf.summary.create_file_writer(train_log_dir)
"""
