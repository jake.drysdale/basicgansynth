#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 14:50:43 2020

@author: jakedrysdale
"""


from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import UpSampling2D, Conv2D, Activation, Input, BatchNormalization, UpSampling1D
from tensorflow.keras.layers import Dense, Reshape, AveragePooling2D, LeakyReLU, Flatten, Conv1D, Conv2DTranspose
from tensorflow.keras.optimizers import RMSprop
import tensorflow.keras.backend as K

import numpy as np
from functools import partial
from matplotlib import pyplot as plt
from datetime import datetime
import os





#class RandomWeightedAverage(_Merge):
#    """
#    Fix batch size
#    """
#    def _merge_function(self, inputs):
#        weights = K.random_uniform((16, 1, 1))
#        return (weights * inputs[0]) + ((1 - weights) * inputs[1])

"""
Fix batch size
"""

class RandomWeightedAverage(tf.keras.layers.Layer):
    def __init__(self):
        super().__init__()
        self._batch_size = 16

    def call(self, inputs, **kwargs):
        alpha = tf.random.uniform((self._batch_size, 1, 1))
        return (alpha * inputs[0]) + ((1 - alpha) * inputs[1])

    def compute_output_shape(self, input_shape):
        return input_shape[0]



class WaveGan(object):
    
    def __init__(self, x_train, latent_size=64, batch_size=16, iterations=50000, 
                 lr=8e-4, start_size=16, end_size=4096, kernel_len=25, n_filters=32, n_chan=3):
        #data
        self._x_train = x_train
        
        #parameters
        self._latent_size = latent_size
        self._batch_size = batch_size
        self._iterations = iterations
        self._lr = lr
        
        #spec/image params
        self._start_size = start_size
        self._end_size = end_size
        self._n_filters = n_filters
        self._n_chan = n_chan
        self._kernel_len = kernel_len
        
        #network building
        self._generator = self.make_generator(self._latent_size)
        self._discriminator = self.make_discriminator()    
        self._DiscriminatorModel = self.build_d_network()
        self._GeneratorModel = self.build_g_network() 
        
        #losses
        self._d_loss = []
        self._g_loss = []
        self._gp_loss = []
        

    def wasserstein_loss(self, y_true, y_pred):
        """
        Wasserstein_loss: seeks to increase the gap between the scores 
        for real and generated images.
        """
        return K.mean(y_true * y_pred)
    

    def r1_gradient_penalty(self, y_true, y_pred, samples, sample_weight):
        """
        Helps prevent exploding gradients and mode collapse issues:
            "Stops the discriminator from telling the generator to change 
            the generated pixel values by too much"
        """
        gradients = K.gradients(y_pred, samples)[0]
        gradients_sqr = K.square(gradients)
        gradient_penalty = K.sum(gradients_sqr,
                                  axis=np.arange(1, len(gradients_sqr.shape)))
        return K.mean(gradient_penalty)
    

    def gradient_penalty_loss(self, y_true, y_pred, averaged_samples, gradient_penalty_weight):
        gradients = K.gradients(K.sum(y_pred), averaged_samples)
        gradient_l2_norm = K.sqrt(K.sum(K.square(gradients)))
        gradient_penalty = gradient_penalty_weight * K.square(1 - gradient_l2_norm)
        return gradient_penalty


    def conv1d_transpose(self,
        inputs,
        filters,
        kernel_width,
        stride=4,
        padding='same'):
        return Conv2DTranspose(
            filters,
            (1, kernel_width),
            strides=(1, stride),
            padding='same'
            )[:, 0](tf.expand_dims(inputs, axis=1))


    def g_block(self, x, filters, kernel_len, stride):
            x = UpSampling1D(stride)(x)
            x = Conv1D(filters, kernel_len, padding = 'same')(x)
            return x
    
    
    def d_block(self, x, filters):
        """
        Not using atm
        """
        x = Conv1D(filters, 3, padding = 'same')(x)
        x = LeakyReLU(0.2)(x)
        x = AveragePooling2D()(x)
        return x

    
    def make_generator(self, latent_size):
        dim_mul = 8
        
        latent_input = Input([latent_size])
                
        #[100] -> [16, 1024]
        x = Dense(self._start_size*self._n_filters*dim_mul, activation = 'relu')(latent_input)
        x = Reshape([self._start_size,self._n_filters*dim_mul])(x)
        x = Activation('relu')(x)
        dim_mul //= 2
        
        #16, 1024] -> [64, 512]
        x = self.g_block(x, self._n_filters*dim_mul, self._kernel_len, 4)
        x = Activation('relu')(x)
        dim_mul //= 2
        
        #[64, 512] -> [256, 256]
        x = self.g_block(x, self._n_filters*dim_mul, self._kernel_len, 4)
        x = Activation('relu')(x)
        dim_mul //= 2
        
        #[256, 256] -> [1024, 128]
        x = self.g_block(x, self._n_filters*dim_mul,self._kernel_len, 4)
        x = Activation('relu')(x)
        dim_mul //= 2
        
        #[1024, 128] -> [4096, 64]
        x = self.g_block(x, self._n_chan, self._kernel_len, 4)
        image_output = Activation('tanh')(x)
        
        #make model
        generator = Model(inputs = latent_input, outputs = image_output)
        
        return generator
    
    
    def make_discriminator(self):
        image_input = Input([self._end_size, self._n_chan])
        
        #[4096, _n_chan] -> [1024, n_filters]
        x = Conv1D(self._n_filters, self._kernel_len, 4, padding = 'same')(image_input)
        x = Activation('relu')(x)
        
        #[1024, n_filters] -> [256, n_filters*2]
        x = Conv1D(self._n_filters*2, self._kernel_len, 4, padding = 'same')(x)
        x = Activation('relu')(x)
        
        #[256, n_filters*2] -> [64, n_filters*4]
        x = Conv1D(self._n_filters*4, self._kernel_len, 4, padding = 'same')(x)
        x = Activation('relu')(x)    
        
        #[64, n_filters*2] -> [16, n_filters*8]
        x = Conv1D(self._n_filters*8, self._kernel_len, 4, padding = 'same')(x)
        x = Activation('relu')(x)  
        x = Flatten()(x)
            
        #1-dimensional neural network
        class_output = Dense(1)(x)
            
        #make model
        discriminator = Model(inputs = image_input, outputs = class_output)
        
        return discriminator

    
    def model_summary(self):
        self._generator.summary()
        self._discriminator.summary()
    
    
# =============================================================================
# TRAINING
# =============================================================================
    def build_d_network(self):
        
#        loss_f = 'mean_squared_error'
#        loss_f = self.wasserstein_loss 
        loss_f = 'binary_crossentropy'
        
        #build network to train the discriminator
        #discriminator will train, but generator won't train
        for layer in self._discriminator.layers:
            layer.trainable = True
            
        for layer in self._generator.layers:
            layer.trainable = False
            
        #get real image
        real_image = Input([self._end_size, self._n_chan])
        
        #discriminator classifies
        validity_real = self._discriminator(real_image)
        
        #get latent input
        latent_input = Input([self._latent_size])
        
        #generate an image
        fake_image = self._generator(latent_input)
        
        #discriminator classifies
        validity_fake = self._discriminator(fake_image)
        
#        #create gradient penalty loss with real samples
#        partial_gp = partial(self.r1_gradient_penalty, samples = real_image)
#        
        
        averaged_samples = RandomWeightedAverage()([real_image, fake_image])
        averaged_samples_out = self._discriminator(averaged_samples)
        
        partial_gp_loss = partial(self.gradient_penalty_loss,
                                      averaged_samples=averaged_samples,
                                      gradient_penalty_weight=1)
        partial_gp_loss.__name__ = 'gradient_penalty'
    
            
        #create and compile the model
        DiscriminatorModel = Model(inputs = [real_image, latent_input], 
                                   outputs = [validity_real, validity_fake, 
                                              averaged_samples_out])
        
        DiscriminatorModel.compile(optimizer = RMSprop(lr = self._lr),
                                   loss = [loss_f, 
                                           loss_f, partial_gp_loss], 
                                           loss_weights = [1, 1, 10])
        return DiscriminatorModel


    def build_g_network(self):
        
        loss_f = 'mean_squared_error'
#        loss_f = self.wasserstein_loss
#        loss_f = 'binary_crossentropy'

        #build network to train the generator
        #discriminator won't train, but generator will train
        for layer in self._discriminator.layers:
            layer.trainable = False
            
        for layer in self._generator.layers:
            layer.trainable = True
        
        #get latent input
        latent_input = Input([self._latent_size])
        
        #generate an image
        fake_image = self._generator(latent_input)
        
        #discriminator classifies
        validity = self._discriminator(fake_image)
        
        #create and compile the model
        GeneratorModel = Model(inputs = latent_input, outputs = validity)
        GeneratorModel.compile(optimizer = RMSprop(lr = self._lr), 
                               loss = loss_f)
        return GeneratorModel


    def train_loop(self):
        #train the models in a loop
        start_time = datetime.now()
        sample_interval=50
        
#        tf.reset_default_graph() 
#        
#        #define metrics
#        train_loss_disc = tf.keras.metrics.Mean('train_loss_disc', dtype=tf.float32)
#        train_loss_gp = tf.keras.metrics.Mean('train_loss_gp', dtype=tf.float32)
#        train_loss_gen = tf.keras.metrics.Mean('train_loss_gen', dtype=tf.float32)
#        init = tf.global_variables_initializer()
#
#
#        with tf.Session() as sess:
#            writer = tf.summary.FileWriter('./graphs', sess.graph)
#            
    
        for i in range(self._iterations):
#                sess.run(init)

                            
            elapsed_time = datetime.now() - start_time
            print('\riteration: ' + str(i) +'  time: '+ str(elapsed_time), end = '')
            
            #get labels
            real_labels = np.ones([self._batch_size, 1])
            fake_labels = np.zeros([self._batch_size, 1])
            dummy_labels = np.ones([self._batch_size, 1])
            
            #train discriminator
            #get images and latent vectors
            image_indices = np.random.randint(0, self._x_train.shape[0] - 1, 
                                              [self._batch_size])
        #    real_images = x_train[image_indices]
        
            real_images=[]
            for i in range(len(image_indices)):
                real_images.append(self._x_train[image_indices[i]])
                
            real_images = tf.convert_to_tensor(real_images)
            latent_vectors = np.random.normal(0.0, 1.0, [self._batch_size, self._latent_size])
            
            #train discriminator
            d_loss = self._DiscriminatorModel.train_on_batch([real_images, latent_vectors], 
                                                           [real_labels, fake_labels, 
                                                            dummy_labels])
            self._d_loss.append(d_loss[1]/2 + d_loss[2]/2)
            self._gp_loss.append(d_loss[3])
            
#
#                train_loss_disc(d_loss[1]/2 + d_loss[2]/2)
#                train_loss_gp(d_loss[3])
#
            
            #train generator
            #get latent vectors
            latent_vectors = np.random.normal(0.0, 1.0, [self._batch_size, self._latent_size])
            
            #train using opposite labels
            g_loss = self._GeneratorModel.train_on_batch(latent_vectors, 
                                                       real_labels)
            self._g_loss.append(g_loss)
#                train_loss_gen(g_loss)

#            print('\rgloss: ' + str(self._g_loss[i]) + '  dloss: '+ str(self._d_loss[i]) + '  gploss: '+ str(self._gp_loss[i]), end = '')
            
            # If at save interval => save generated image samples
            if i % sample_interval == 0:
                self.sample_generations(i)
            
            filename = 'generator_model_%03d.h5' % (i+1)
            self._GeneratorModel.save('/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/generator_models/'+filename)
            
#                gen_loss, disc_loss, gp_loss = sess.run([tf.summary.scalar('gen_loss', train_loss_gen.result()), 
#                                                         tf.summary.scalar('disc_loss', train_loss_disc.result()),
#                                                         tf.summary.scalar('gp_loss', train_loss_gp.result())])
#                
#                
#                writer.add_summary(gen_loss, i)
#                writer.add_summary(disc_loss, i)
#                writer.add_summary(gp_loss, i)
#                writer.flush()
                
    
    
    def sample_generations(self, iteration):
#        os.makedirs('./images/%s' % 'kicks', exist_ok=True)
        path = '/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/basicgansynth/generations'
        gen_amount=10
        latent_vectors = np.random.normal(0.0, 1.0, [gen_amount, self._latent_size])
        fake_images = gan._generator.predict(latent_vectors)
        np.save(path+'/generations_'+str(iteration)+'.npy',fake_images)
        # show samples
        for i in range(gen_amount):
#            rand_indx = np.random.randint(len(fake_images))
            fig = plt.figure(i)
            plt.plot(fake_images[i,:])
            fig.savefig(path+"/%s/%d_%d.png" % ('kicks', iteration, i))
            plt.close()
            
        
            
    #    with train_summary_writer.as_default():
    #      tf.summary.scalar('disc_loss', d_loss, step=i)
    #      tf.summary.scalar('gen_loss', g_loss, step=i)
    
    
    def plot_losses(self):
        
        # plot losses
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.plot(self._d_loss,label='d_loss')
    #    ax2 = fig.add_subplot(111)
        ax1.plot(self._g_loss,label='g_loss')
        plt.legend()
        plt.title('Generator and Discriminator Losses')
        plt.xlabel('Iterations')
        plt.ylabel('Loss')
        
#        now = datetime.now()
#        current_time = now.strftime("%H:%M:%S")
#        loss_dir = '/Users/s15125210/Documents/PhD/gitlab/kickGAN/loss_graphs/'
#        fig.savefig(loss_dir+current_time+'_dg_loss'+'.png')
















#create model
gan = WaveGan(x_train)
summary = gan.model_summary()


train = gan.train_loop() #run GAN training on x_train




#x_train[index, samples, channels]













# =============================================================================
# generation stuff
# =============================================================================



def generate_samples(gen_amount=100):
    # generate samples
    latent_vectors = np.random.normal(0.0, 1.0, [gen_amount, gan._latent_size])
    fake_images = gan._generator.predict(latent_vectors)
    generated_audio = [np.flip(x) for x in fake_images]
    generated_audio = np.array(generated_audio)
    return generated_audio



generated_audio = generate_samples(gen_amount=500)

#np.save('/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/generations'+'/generations-feb-22-satnight.npy',generated_audio)




def flipphase(audio): 
    return [ -i for i in audio] 

def normalise(audio):
    return audio/np.max(np.abs(audio))




from scipy.signal import savgol_filter #cleans up noisy low freq signals

def combine_samples(generated_audio):
    """
    combine the generated audio (low, mid, high)
    """
    
    low = generated_audio[:,:,2]
    mid = generated_audio[:,:,1]
    high = generated_audio[:,:,0]
    
    fade = np.linspace(0,1,20)
    pad = np.ones((len(low[0])-20))
    attack = np.append(fade,pad)
    
    combined_audio=[]
    for i in range(len(generated_audio)):
        combined = normalise(savgol_filter(low[i],13,1)+savgol_filter(flipphase(mid[i]),13,1)+high[i])
        attack_audio = combined*attack
        combined_audio.append(attack_audio)
        
    return combined_audio



from librosa.output import write_wav
import sounddevice as  sd


combined_audio = combine_samples(generated_audio)
out_path = '/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/basicgansynth/generations/22feb_comi_generations/'

for i in range(len(combined_audio)):
    write_wav(out_path+str(i)+'.wav', combined_audio[i], sr =16000)









def save_generationsas_wav(generated_audio, output_dir):
    for i in range(len(generated_audio)):
        write_wav(output_dir+str(i)+'.wav',np.array(generated_audio[i,:,2]),sr=16000)


sd.play(generated_audio[1,:],samplerate=16000)

save_generationsas_wav(generated_audio,'/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/generations/wavs-feb22/')













# =============================================================================
#interpolation stuff
# =============================================================================


# spherical linear interpolation (slerp)
def slerp(val, low, high):
	omega = np.arccos(np.clip(np.dot(low/np.linalg.norm(low), high/np.linalg.norm(high)), -1, 1))
	so = np.sin(omega)
	if so == 0:
		# L'Hopital's rule/LERP
		return (1.0-val) * low + val * high
	return np.sin((1.0-val)*omega) / so * low + np.sin(val*omega) / so * high


def interpolate_points(p1, p2, n_steps=20):
	# interpolate ratios between the points
	ratios = np.linspace(0, 2, num=n_steps)
	# linear interpolate vectors
	vectors = list()
	for ratio in ratios:
		v = slerp(ratio, p1, p2)
		vectors.append(v)
	return np.asarray(vectors)


def create_interpolation_images(n=2):
    """
    n = resolution?
    """
    latent_vectors = np.random.normal(0.0, 1.0, [n, gan._latent_size])
    interpolated = interpolate_points(latent_vectors[0], latent_vectors[5])
    
    results = gan._generator.predict(interpolated)
    results = np.flip(results)
    
    for i in range(len(results)):
        fig = plt.figure()
        plt.plot(results[i,:,0])
        plt.ylim(-1,1)
        plt.xlim(0,4096)
        fig.savefig('/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/interpolations2/'+'generation'+str(i)+'.png')
        plt.close()

create_interpolation_images()






# =============================================================================
# plots 
# =============================================================================




# show samples
for i in range(20):
    rand_indx = np.random.randint(len(generated_audio))
    plt.figure(i)
    plt.plot(generated_audio[i,:,2])



def generate_joint_plot(generated_audio, real_audio):
    rand_indx = np.random.randint(len(generated_audio))
    fig, (ax1, ax2) = plt.subplots(2, 1, sharey=True)
    fig.tight_layout(pad=2)
    ax1.plot(generated_audio[rand_indx])
    ax1.set_title('Generated drum hit')
    ax2.plot(real_audio[rand_indx])
    ax2.set_title('Real drum hit')
    



    
    
now = datetime.now()
current_time = now.strftime("%H:%M:%S")
generations_dir= '/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/generations/'
np.save(generations_dir+current_time+'.npy', fake_images)



for i in range(len(audio[:20])):
    plt.figure(i)
    plt.plot(audio[i,:])

#from matplotlib import pyplot as plt
#from datetime import datetime



loss_dir = '/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/graphs/'

# plot losses
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(gan._d_loss,label='d_loss')

ax2 = fig.add_subplot(111)
ax1.plot(gan._g_loss,label='g_loss')
plt.legend()
plt.title('Generator and Discriminator Losses')
plt.xlabel('Iterations')
plt.ylabel('Loss')

fig.savefig(loss_dir+current_time+'_dg_loss'+'.png')



fig2 = plt.figure()
ax1 = fig2.add_subplot(111)
ax1.plot(gan._gp_loss,label='gp_loss')
plt.legend()
plt.title('Gradient Penalty Loss')
plt.xlabel('Iterations')
plt.ylabel('Loss')
#
fig2.savefig(loss_dir+current_time+'_gp_loss'+'.png')
#





logdir = "logs/scalars/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)







# =============================================================================
# 
# =============================================================================

"""
#set up summary writers to write the summaries to disk in a different logs directory:
current_time = datetime.now().strftime("%Y%m%d-%H%M%S")
#cwd = os.getcwd()
train_log_dir = './logs/' + current_time + '/train'
train_summary_writer = tf.summary.create_file_writer(train_log_dir)
"""
