#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 11:24:02 2020

@author: Jake Drysdales
"""
import os

def split_frequencies(input_dir, output_dir, low_f=120, mid_f=2500):
    sox_dir = '/Users/s15125210/sox-14.4.2/sox'
    
    
    files = [file_i
             for file_i in os.listdir(input_dir)
             if file_i.endswith('.wav')]
    effect1='120lowpass'
    for file in files:
        f_name = (os.path.splitext(file)[0]+'_'+effect1+'.wav')
        command = (sox_dir +' '+ input_dir+file +' '+ output_dir + effect1 + '/' + f_name +' '+ 'lowpass' +' '+ str(low_f))
        print(command)
        os.system(command)
        
    effect2='120_2500bandpass'
    for file in files:
        f_name = (os.path.splitext(file)[0]+'_'+effect2+'.wav')
        command = (sox_dir +' '+ input_dir+file +' '+ output_dir + effect2 + '/' + f_name +' ' + 'lowpass' +' '+ str(mid_f) +' ' + 'highpass' +' '+ str(low_f))
        print(command)
        os.system(command)
        
    effect3='2500highpass'
    for file in files:
        f_name = (os.path.splitext(file)[0]+'_'+effect3+'.wav')
        command = (sox_dir +' '+ input_dir+file +' '+ output_dir + effect3 + '/' + f_name +' '+ 'highpass' +' '+ str(mid_f))
        print(command)
        os.system(command)
        

input_dir = '/Users/s15125210/Desktop/Samples-from-Matt-C/Kicks/'
output_dir =  '/Users/s15125210/Documents/PhD/gitlab/BasicGANSynth/audio_data/filtered_wavs/'

split_frequencies(input_dir, output_dir)